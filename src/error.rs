use std::{error, fmt::Display};

#[derive(Debug)]
pub enum Error {}

impl Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

impl error::Error for Error {}

impl From<async_std::io::Error> for Error {
	fn from(_err: async_std::io::Error) -> Self {
		todo!()
	}
}
