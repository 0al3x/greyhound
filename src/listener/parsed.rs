#[cfg(unix)]
use crate::listener::unix;

use crate::{
	listener::{self, tcp, ListenInfo},
	server::{state::State, Server},
};
use async_std::io;
use async_trait::async_trait;
use std::fmt::{self, Debug, Display, Formatter};

pub enum Listener<S: State> {
	#[cfg(unix)]
	Unix(unix::Listener<S>),
	Tcp(tcp::Listener<S>),
}

impl<S: State> Debug for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			#[cfg(unix)]
			Listener::Unix(unix) => Debug::fmt(unix, f),
			Listener::Tcp(tcp) => Debug::fmt(tcp, f),
		}
	}
}

impl<S: State> Display for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match self {
			#[cfg(unix)]
			Self::Unix(u) => write!(f, "{u}"),
			Self::Tcp(t) => write!(f, "{t}"),
		}
	}
}

#[async_trait]
impl<S: State> listener::Listener<S> for Listener<S>
{
	async fn bind(&mut self, server: Server<S>) -> io::Result<()> {
		match self {
			#[cfg(unix)]
			Self::Unix(u) => u.bind(server).await,
			Self::Tcp(t) => t.bind(server).await,
		}
	}

	async fn accept(&mut self) -> io::Result<()> {
		match self {
			#[cfg(unix)]
			Self::Unix(u) => u.accept().await,
			Self::Tcp(t) => t.accept().await,
		}
	}

	fn info(&self) -> Vec<ListenInfo> {
		match self {
			#[cfg(unix)]
			Listener::Unix(unix) => unix.info(),
			Listener::Tcp(tcp) => tcp.info(),
		}
	}
}
