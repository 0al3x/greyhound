use crate::{
	listener::{self, to_listener::ToListener, ListenInfo},
	server::{state::State, Server},
};
use async_std::io;
use async_trait::async_trait;
use futures_util::stream::{futures_unordered::FuturesUnordered, StreamExt};
use std::fmt::{self, Debug, Display, Formatter};

#[derive(Default)]
pub struct Listener<S: State> {
	listeners: Vec<Box<dyn listener::Listener<S>>>,
}

impl<S: State> Listener<S> {
	#[must_use]
	pub fn new() -> Self {
		Self { listeners: vec![] }
	}

	/// # Errors
	///
	/// fails when listener cannot be parse into a concrete listener
	pub fn add<L>(&mut self, listener: L) -> io::Result<()>
	where
		L: ToListener<S>,
	{
		self.listeners.push(Box::new(listener.to_listener()?));
		Ok(())
	}

	/// # Panics
	///
	/// if (add)[`Self::add`] fails adding the listener
	#[must_use]
	pub fn with_listener<L>(mut self, listener: L) -> Self
	where
		L: ToListener<S>,
	{
		self.add(listener).expect("Unable to add listener");
		self
	}
}

#[async_trait]
impl<S: State> listener::Listener<S> for Listener<S> {
	/// # Errors
	///
	/// if fails at bingind the listener to server
	async fn bind(&mut self, app: Server<S>) -> io::Result<()> {
		for listener in &mut self.listeners {
			listener.bind(app.clone()).await?;
		}
		Ok(())
	}

	async fn accept(&mut self) -> io::Result<()> {
		let mut futures_unordered = FuturesUnordered::new();

		for listener in &mut self.listeners {
			futures_unordered.push(listener.accept());
		}

		while let Some(result) = futures_unordered.next().await {
			result?;
		}
		Ok(())
	}

	fn info(&self) -> Vec<ListenInfo> {
		self.listeners
			.iter()
			.flat_map(|listener| listener.info().into_iter())
			.collect()
	}
}

impl<S: State> Debug for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self.listeners)
	}
}

impl<S: State> Display for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		let string = self
			.listeners
			.iter()
			.map(std::string::ToString::to_string)
			.collect::<Vec<_>>()
			.join(", ");

		writeln!(f, "{string}")
	}
}
