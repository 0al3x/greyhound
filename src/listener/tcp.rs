use crate::{
	listener::{self, is_transient_error, ListenInfo},
	server::{state::State, Server},
};
use async_std::{
	io,
	net::{self, SocketAddr, TcpStream},
	stream::StreamExt,
	task,
};
use async_trait::async_trait;
use log::error;
use std::fmt::{Debug, Display};

pub struct Listener<S: State> {
	addrs: Option<Vec<SocketAddr>>,
	listener: Option<net::TcpListener>,
	server: Option<Server<S>>,
	info: Option<ListenInfo>,
}

impl<S: State> Listener<S> {
	pub fn from_addrs(addrs: Vec<SocketAddr>) -> Self {
		Self {
			addrs: Some(addrs),
			listener: None,
			server: None,
			info: None,
		}
	}

	pub fn from_listener(tcp_listener: impl Into<net::TcpListener>) -> Self {
		Self {
			addrs: None,
			listener: Some(tcp_listener.into()),
			server: None,
			info: None,
		}
	}
}

fn handle_tcp<S: State>(app: Server<S>, stream: TcpStream) {
	task::spawn(async move {
		let local_addr = stream.local_addr().ok();
		let peer_addr = stream.peer_addr().ok();

		let fut = async_h1::accept(stream, |mut req| async {
			req.set_local_addr(local_addr);
			req.set_peer_addr(peer_addr);
			app.process(req).await
		});

		if let Err(error) = fut.await {
			eprintln!("async-h1 error {error}");
		}
	});
}

#[async_trait]
impl<S: State> listener::Listener<S> for Listener<S> {
	async fn bind(&mut self, server: Server<S>) -> io::Result<()> {
		assert!(self.server.is_none(), "`bind` should only be called once");
		self.server = Some(server);

		if self.listener.is_none() {
			let addrs = self
				.addrs
				.take()
				.expect("`bind` should only be called once");
			let listener = net::TcpListener::bind(addrs.as_slice()).await?;
			self.listener = Some(listener);
		}

		// Format the listen information.
		let conn_string = format!("{__self}");
		let transport = "tcp".to_owned();
		let tls = false;
		self.info = Some(ListenInfo::new(conn_string, transport, tls));

		Ok(())
	}

	async fn accept(&mut self) -> io::Result<()> {
		let server = self
			.server
			.take()
			.expect("`Listener::bind` must be called before `Listener::accept`");
		let listener = self
			.listener
			.take()
			.expect("`Listener::bind` must be called before `Listener::accept`");

		let mut incoming = listener.incoming();

		while let Some(stream) = incoming.next().await {
			match stream {
				Err(ref e) if is_transient_error(e) => continue,
				Err(error) => {
					let delay = std::time::Duration::from_millis(500);
					error!("Error: {}. Pausing for {:?}.", error, delay);
					task::sleep(delay).await;
					continue;
				}

				Ok(stream) => {
					handle_tcp(server.clone(), stream);
				}
			};
		}
		Ok(())
	}

	fn info(&self) -> Vec<ListenInfo> {
		match &self.info {
			Some(info) => vec![info.clone()],
			None => vec![],
		}
	}
}

impl<S: State> Debug for Listener<S> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Listener")
			.field("listener", &self.listener)
			.field("addrs", &self.addrs)
			.field("indo", &self.info)
			.field(
				"server",
				if self.server.is_some() {
					&"Some(Server<State>)"
				} else {
					&"None"
				},
			)
			.finish()
	}
}

impl<S: State> Display for Listener<S> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let http_fmt = |a| format!("http://{a}");
		match &self.listener {
			Some(listener) => {
				let addr = listener.local_addr().expect("Could not get local addr");
				write!(f, "{}", http_fmt(&addr))
			}
			None => match &self.addrs {
				Some(addrs) => {
					let addrs = addrs.iter().map(http_fmt).collect::<Vec<_>>().join(", ");
					write!(f, "{addrs}")
				}
				None => write!(f, "Not listening. Did you forget to call `Listener::bind`?"),
			},
		}
	}
}
