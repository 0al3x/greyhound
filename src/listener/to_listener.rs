use crate::{listener::Listener, server::state::State};
use async_std::io;

pub trait ToListener<S: State> {
	type Listener: Listener<S>;

	/// # Errors
	///
	///
	fn to_listener(self) -> io::Result<Self::Listener>;
}
