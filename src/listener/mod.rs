use crate::server::{state::State, Server};
use async_std::io;
use async_trait::async_trait;
use std::fmt::{Debug, Display};

pub mod concurrent;
pub mod failover;
#[cfg(feature = "h1-server")]
mod parsed;
#[cfg(feature = "h1-server")]
mod tcp;
mod to_listener;
#[cfg(feature = "h1-server")]
mod to_listener_impls;
#[cfg(all(unix, feature = "h1-server"))]
mod unix;

#[allow(clippy::module_name_repetitions)]
pub use to_listener::ToListener;

/*#[cfg(feature = "h1-server")]
pub(crate) use parsed::Listener as ParsedListener;
#[cfg(feature = "h1-server")]
pub(crate) use tcp::Listener as TcpListener;
#[cfg(all(unix, feature = "h1-server"))]
pub(crate) use unix::Listener as UnixListener;*/

#[async_trait]
pub trait Listener<S>: Debug + Display + Send + Sync + 'static
where
	S: State,
{
	async fn bind(&mut self, app: Server<S>) -> io::Result<()>;

	async fn accept(&mut self) -> io::Result<()>;

	fn info(&self) -> Vec<ListenInfo>;
}

#[async_trait]
impl<L, S> Listener<S> for Box<L>
where
	L: Listener<S>,
	S: State,
{
	async fn bind(&mut self, app: Server<S>) -> io::Result<()> {
		self.as_mut().bind(app).await
	}

	async fn accept(&mut self) -> io::Result<()> {
		self.as_mut().accept().await
	}

	fn info(&self) -> Vec<ListenInfo> {
		self.as_ref().info()
	}
}

/// crate-internal shared logic used by tcp and unix listeners to
/// determine if an `io::Error` needs a backoff delay. Transient error
/// types do not require a delay.
// #[cfg(feature = "h1-server")]
pub(crate) fn is_transient_error(e: &io::Error) -> bool {
	use io::ErrorKind::{ConnectionAborted, ConnectionRefused, ConnectionReset};

	matches!(
		e.kind(),
		ConnectionRefused | ConnectionAborted | ConnectionReset
	)
}

/// Information about the `Listener`.
///
/// See [`Report`](../listener/trait.Report.html) for more.
#[derive(Debug, Clone)]
pub struct ListenInfo {
	conn_string: String,
	transport: String,
	tls: bool,
}

impl ListenInfo {
	/// Create a new instance of `ListenInfo`.
	///
	/// This method should only be called when implementing a new Tide `listener`
	/// strategy.
	#[must_use]
	pub fn new(conn_string: String, transport: String, tls: bool) -> Self {
		Self {
			conn_string,
			transport,
			tls,
		}
	}

	/// Get the connection string.
	#[must_use]
	pub fn connection(&self) -> &str {
		self.conn_string.as_str()
	}

	/// The underlying transport this connection listens on.
	///
	/// Examples are: "tcp", "uds", etc.
	#[must_use]
	pub fn transport(&self) -> &str {
		self.transport.as_str()
	}

	/// Is the connection encrypted?
	#[must_use]
	pub fn is_encrypted(&self) -> bool {
		self.tls
	}
}

impl Display for ListenInfo {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.conn_string)
	}
}
