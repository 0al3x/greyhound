use crate::{
	http::Url,
	listener::{concurrent, failover, parsed, tcp, to_listener::ToListener, unix},
	server::state::State,
};
use async_std::io;
use std::net::ToSocketAddrs;

impl<S: State> ToListener<S> for Url
{
	type Listener = parsed::Listener<S>;

	fn to_listener(self) -> io::Result<Self::Listener> {
		match self.scheme() {
			"http+unix" => {
				#[cfg(unix)]
				{
					let path = std::path::PathBuf::from(format!(
						"{}{}",
						self.domain().unwrap_or_default(),
						self.path()
					));

					Ok(parsed::Listener::Unix(unix::Listener::from_path(path)))
				}

				#[cfg(not(unix))]
				{
					Err(io::Error::new(
						io::ErrorKind::Other,
						"Unix sockets not supported on this platform",
					))
				}
			}

			"tcp" | "http" => Ok(parsed::Listener::Tcp(tcp::Listener::from_addrs(
				self.socket_addrs(|| Some(80))?,
			))),

			"tls" | "ssl" | "https" => Err(io::Error::new(
				io::ErrorKind::Other,
				"parsing TLS listeners not supported yet",
			)),

			_ => Err(io::Error::new(
				io::ErrorKind::InvalidInput,
				"unrecognized url scheme",
			)),
		}
	}
}

impl<S: State> ToListener<S> for String
{
	type Listener = parsed::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		ToListener::<S>::to_listener(self.as_str())
	}
}

impl<S: State> ToListener<S> for &String
{
	type Listener = parsed::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		ToListener::<S>::to_listener(self.as_str())
	}
}

impl<S: State> ToListener<S> for &str
{
	type Listener = parsed::Listener<S>;

	fn to_listener(self) -> io::Result<Self::Listener> {
		if let Ok(socket_addrs) = self.to_socket_addrs() {
			Ok(parsed::Listener::Tcp(tcp::Listener::from_addrs(
				socket_addrs.collect(),
			)))
		} else if let Ok(url) = Url::parse(self) {
			ToListener::<S>::to_listener(url)
		} else {
			Err(io::Error::new(
				io::ErrorKind::InvalidInput,
				format!("unable to parse listener from `{self}`"),
			))
		}
	}
}

#[cfg(unix)]
impl<S: State> ToListener<S> for async_std::path::PathBuf
{
	type Listener = unix::Listener<S>;

	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(unix::Listener::from_path(self))
	}
}

#[cfg(unix)]
impl<S: State> ToListener<S> for std::path::PathBuf
{
	type Listener = unix::Listener<S>;

	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(unix::Listener::from_path(self))
	}
}

impl<S: State> ToListener<S> for async_std::net::TcpListener
{
	type Listener = tcp::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(tcp::Listener::from_listener(self))
	}
}

impl<S: State> ToListener<S> for std::net::TcpListener

{
	type Listener = tcp::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(tcp::Listener::from_listener(self))
	}
}

impl<S: State> ToListener<S> for (String, u16)
{
	type Listener = tcp::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		ToListener::<S>::to_listener((self.0.as_str(), self.1))
	}
}

impl<S: State> ToListener<S> for (&String, u16)
{
	type Listener = tcp::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		ToListener::<S>::to_listener((self.0.as_str(), self.1))
	}
}

impl<S: State> ToListener<S> for (&str, u16)
{
	type Listener = tcp::Listener<S>;

	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(tcp::Listener::from_addrs(self.to_socket_addrs()?.collect()))
	}
}

#[cfg(unix)]
impl<S: State> ToListener<S> for async_std::os::unix::net::UnixListener
{
	type Listener = unix::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(unix::Listener::from_listener(self))
	}
}

#[cfg(unix)]
impl<S: State> ToListener<S> for std::os::unix::net::UnixListener
{
	type Listener = unix::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(unix::Listener::from_listener(self))
	}
}

impl<S: State> ToListener<S> for tcp::Listener<S>
{
	type Listener = Self;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(self)
	}
}

#[cfg(unix)]
impl<S: State> ToListener<S> for unix::Listener<S>
{
	type Listener = Self;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(self)
	}
}

impl<S: State> ToListener<S> for concurrent::Listener<S>
{
	type Listener = Self;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(self)
	}
}

impl<S: State> ToListener<S> for parsed::Listener<S>
{
	type Listener = Self;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(self)
	}
}

impl<S: State> ToListener<S> for failover::Listener<S>
{
	type Listener = Self;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(self)
	}
}

impl<S: State> ToListener<S> for std::net::SocketAddr
{
	type Listener = tcp::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		Ok(tcp::Listener::from_addrs(vec![self]))
	}
}

impl<L, S> ToListener<S> for Vec<L>
where
	L: ToListener<S>,
	S: State,
{
	type Listener = concurrent::Listener<S>;
	fn to_listener(self) -> io::Result<Self::Listener> {
		let mut concurrent_listener = concurrent::Listener::new();
		for listener in self {
			concurrent_listener.add(listener)?;
		}
		Ok(concurrent_listener)
	}
}
