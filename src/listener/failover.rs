use crate::{
	listener::{self, to_listener::ToListener, ListenInfo},
	server::{state::State, Server},
};
use async_std::io;
use async_trait::async_trait;
use log::info;
use std::fmt::{self, Debug, Display, Formatter};

#[derive(Default)]
pub struct Listener<S: State> {
	listeners: Vec<Option<Box<dyn listener::Listener<S>>>>,
	index: Option<usize>,
}

impl<S: State> Listener<S> {
	#[must_use]
	pub fn new() -> Self {
		Self {
			listeners: vec![],
			index: None,
		}
	}

	/// # Errors
	///
	///
	pub fn add<L>(&mut self, listener: L) -> io::Result<()>
	where
		L: ToListener<S>,
	{
		self.listeners.push(Some(Box::new(listener.to_listener()?)));
		Ok(())
	}

	/// # Panics
	///
	///
	#[must_use]
	pub fn with_listener<L>(mut self, listener: L) -> Self
	where
		L: ToListener<S>,
	{
		self.add(listener).expect("Unable to add listener");
		self
	}
}

#[async_trait]
impl<S: State> listener::Listener<S> for Listener<S> {
	async fn bind(&mut self, app: Server<S>) -> io::Result<()> {
		for (index, listener) in self.listeners.iter_mut().enumerate() {
			let listener = listener.as_deref_mut().expect("bind called twice");
			match listener.bind(app.clone()).await {
				Ok(()) => {
					self.index = Some(index);
					return Ok(());
				}
				Err(e) => {
					info!("unable to bind {listener}\n{e}");
				}
			}
		}

		Err(io::Error::new(
			io::ErrorKind::AddrNotAvailable,
			"unable to bind to any supplied listener spec",
		))
	}

	async fn accept(&mut self) -> io::Result<()> {
		match self.index {
			Some(index) => {
				let mut listener = self.listeners[index].take().expect("accept called twice");
				listener.accept().await?;
				Ok(())
			}
			None => Err(io::Error::new(
				io::ErrorKind::AddrNotAvailable,
				"unable to listen to any supplied listener spec",
			)),
		}
	}

	fn info(&self) -> Vec<ListenInfo> {
		match self.index {
			Some(index) => match self.listeners.get(index) {
				Some(Some(listener)) => listener.info(),
				_ => vec![],
			},
			None => vec![],
		}
	}
}

impl<S: State> Debug for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(f, "{:?}", self.listeners)
	}
}

impl<S: State> Display for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		let string = self
			.listeners
			.iter()
			.map(|l| match l {
				Some(l) => l.to_string(),
				None => String::new(),
			})
			.collect::<Vec<_>>()
			.join(", ");

		writeln!(f, "{string}")
	}
}
