use crate::{
	listener::{self, is_transient_error, ListenInfo},
	server::{state::State, Server},
};
use async_std::{
	os::unix::net::{self, SocketAddr, UnixStream},
	path::PathBuf,
	stream::StreamExt,
	{io, task},
};
use async_trait::async_trait;
use log::error;
use std::fmt::{self, Display, Formatter};

pub struct Listener<S: State> {
	path: Option<PathBuf>,
	listener: Option<net::UnixListener>,
	server: Option<Server<S>>,
	info: Option<ListenInfo>,
}

impl<S: State> Listener<S> {
	pub fn from_path(path: impl Into<PathBuf>) -> Self {
		Self {
			path: Some(path.into()),
			listener: None,
			server: None,
			info: None,
		}
	}

	pub fn from_listener(unix_listener: impl Into<net::UnixListener>) -> Self {
		Self {
			path: None,
			listener: Some(unix_listener.into()),
			server: None,
			info: None,
		}
	}
}

fn handle_unix<S: State>(app: Server<S>, stream: UnixStream) {
	task::spawn(async move {
		let local_addr = unix_socket_addr_to_string(stream.local_addr());
		let peer_addr = unix_socket_addr_to_string(stream.peer_addr());

		let fut = async_h1::accept(stream, |mut req| async {
			req.set_local_addr(local_addr.as_ref());
			req.set_peer_addr(peer_addr.as_ref());
			app.process(req).await
		});

		if let Err(error) = fut.await {
			error!("async-h1 error {error}");
		}
	});
}

#[async_trait]
impl<S: State> listener::Listener<S> for Listener<S> {
	async fn bind(&mut self, server: Server<S>) -> io::Result<()> {
		assert!(self.server.is_none(), "`bind` should only be called once");
		self.server = Some(server);

		if self.listener.is_none() {
			let path = self.path.take().expect("`bind` should only be called once");
			let listener = net::UnixListener::bind(path).await?;
			self.listener = Some(listener);
		}

		// Format the listen information.
		let conn_string = format!("{__self}");
		let transport = "uds".to_owned();
		let tls = false;
		self.info = Some(ListenInfo::new(conn_string, transport, tls));

		Ok(())
	}

	async fn accept(&mut self) -> io::Result<()> {
		let server = self
			.server
			.take()
			.expect("`Listener::bind` must be called before `Listener::accept`");
		let listener = self
			.listener
			.take()
			.expect("`Listener::bind` must be called before `Listener::accept`");

		let mut incoming = listener.incoming();

		while let Some(stream) = incoming.next().await {
			match stream {
				Err(ref e) if is_transient_error(e) => continue,
				Err(error) => {
					let delay = std::time::Duration::from_millis(500);
					error!("Error: {}. Pausing for {:?}.", error, delay);
					task::sleep(delay).await;
					continue;
				}

				Ok(stream) => {
					handle_unix(server.clone(), stream);
				}
			};
		}
		Ok(())
	}

	fn info(&self) -> Vec<ListenInfo> {
		match &self.info {
			Some(info) => vec![info.clone()],
			None => vec![],
		}
	}
}

impl<S: State> fmt::Debug for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		f.debug_struct("UnixListener")
			.field("listener", &self.listener)
			.field("path", &self.path)
			.field("info", &self.info)
			.field(
				"server",
				if self.server.is_some() {
					&"Some(Server<State>)"
				} else {
					&"None"
				},
			)
			.finish()
	}
}

impl<S: State> Display for Listener<S> {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		match &self.listener {
			Some(listener) => {
				let path = listener.local_addr().expect("Could not get local path dir");
				let pathname = path
					.as_pathname()
					.and_then(|p| p.canonicalize().ok())
					.expect("Could not canonicalize path dir");
				write!(f, "http+unix://{}", pathname.display())
			}
			None => match &self.path {
				Some(path) => write!(f, "http+unix://{}", path.display()),
				None => write!(f, "Not listening. Did you forget to call `Listener::bind`?"),
			},
		}
	}
}

fn unix_socket_addr_to_string(result: io::Result<SocketAddr>) -> Option<String> {
	result
		.ok()
		.as_ref()
		.and_then(SocketAddr::as_pathname)
		.and_then(|p| p.canonicalize().ok())
		.map(|pathname| format!("http+unix://{}", pathname.display()))
}
