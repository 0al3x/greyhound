pub trait State: Clone + Send + Sync + 'static {}

impl<T> State for T where T: Clone + Send + Sync + 'static {}
