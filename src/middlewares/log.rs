use crate::{
	endpoint::HandlerOutput,
	middleware::{Middleware, Next},
	request::Request,
	server::state::State,
};
use log::{error, info};

#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Default, Clone)]
pub struct LogMiddleware;

struct LogMiddlewareHasBeenRun;

impl LogMiddleware {
	#[must_use]
	pub fn new() -> Self {
		Self
	}

	async fn log<'a, S: State>(
		&'a self,
		mut req: Request<S>,
		next: Next<'a, S, Request<S>, HandlerOutput>,
	) -> HandlerOutput {
		if req.ext().get::<LogMiddlewareHasBeenRun>().is_some() {
			return next.run(req).await;
		}
		req.ext_mut().insert(LogMiddlewareHasBeenRun);
		let path = req.url().path().to_owned();
		let method = req.method().to_string();

		info!("\n<-- Request received\n\tmethod: {method}\n\tpath: {path}\n\n");

		let start = std::time::Instant::now();

		match next.run(req).await {
			Ok(response) => {
				let status = response.status();

				if status.is_success() {
					info!("\n--> Response sent\n\tmethod: {method}\n\tpath: {path}\n\tstatus: {}\n\tduration: {}\n\n",
						format!("{} - {}", status as u16, status.canonical_reason()),
						format!("{:?}", start.elapsed())
					);
				} else {
					error!(
						"\nInternal error --> Response sent\n\tmethod: {method}\n\tpath: {path}\n\tstatus: {}\n\tduration: {}\n\n",
						format!("{} - {}", status as u16, status.canonical_reason()),
						format!("{:?}", start.elapsed())
					);
				}
				Ok(response)
			}
			Err(e) => {
				error!("ERROR:\n{e:#?}");
				Err(e)
			}
		}
	}
}

#[async_trait::async_trait]
impl<S: State> Middleware<S, Request<S>, HandlerOutput> for LogMiddleware {
	async fn handle(
		&self,
		req: Request<S>,
		next: Next<'_, S, Request<S>, HandlerOutput>,
	) -> HandlerOutput {
		self.log(req, next).await
	}
}
