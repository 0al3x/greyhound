use greyhound::{
	// endpoint::HandlerOutput,
	http::{Error, Response},
	request::Request,
	server::Server,
};
use std::sync::{
	atomic::{AtomicU32, Ordering},
	Arc,
};

#[derive(Clone, Debug, Default)]
pub struct ServerState {
	visits: Arc<AtomicU32>,
}

async fn state(request: Request<ServerState>) -> Result<Response, Error> {
	Ok(format!("{:#?}", request.state()).into())
}

async fn increment(request: Request<ServerState>) -> Result<Response, Error> {
	let visits = request.state().visits.fetch_add(1, Ordering::Relaxed) + 1;
	Ok(format!("Visits: {visits}").into())
}

#[async_std::main]
async fn main() {
	std::env::set_var("RUST_LOG", "debug");
	env_logger::init();

	let mut server = Server::<ServerState>::default();

	server.at("/").get(state).await;
	server.at("/increment").post(increment).await;

	server.listen("127.0.0.1:9090").await.unwrap();
}
