#![allow(clippy::missing_errors_doc, clippy::missing_panics_doc)]

use async_std::io;
use greyhound::{
	http::{Error, Response, StatusCode},
	middleware::{After, Before},
	middlewares::log::LogMiddleware,
	request::Request,
	response,
	server::Server,
};

const HELLO: &str = r#"<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Hello!</title>
        <style>
            * { white-space: pre; }
        </style>
	</head>
	<body>
		<h1>Hello!</h1>
		<p>Hi from Rust</p>
	</body>
</html>"#;

pub async fn before(req: Request<()>) -> Result<Request<()>, Error> {
	log::debug!("before middleware");
	Ok(req)
}

pub async fn after(res: response::Response) -> Result<Response, Error> {
	let mut res = res;
	let mut body = res.body_string().await.unwrap();
	body.push_str("\n modificado despues del middleware");
	res.set_body(body);
	Ok(res.into())
}

async fn hello_world(_: Request<()>) -> Result<Response, Error> {
	let mut res = Response::new(StatusCode::Ok);
	res.insert_header("Content-Type", "text/html");
	res.set_body(HELLO);
	Ok(res)
}

#[async_std::main]
async fn main() -> io::Result<()> {
	std::env::set_var("RUST_LOG", "debug");
	env_logger::init();

	let mut server = Server::new();

	server.with(LogMiddleware::new()).await;

	let mut hello_route = server.at("/");

	hello_route
		.with(After(after))
		.await
		.with(Before(before))
		.await
		.get(hello_world)
		.await;

	server.listen("127.0.0.1:9090").await
}
