use greyhound::{
	http::{self, Response},
	middleware::Next,
	request::Request,
	server::Server,
};
use std::{future::Future, pin::Pin};

fn app_mid<'a>(
	req: Request<()>,
	next: Next<'a, (), Request<()>, Result<Response, http::Error>>,
) -> Pin<Box<dyn Future<Output = Result<Response, http::Error>> + Send + 'a>> {
	log::info!("inside app middleware");

	Box::pin(async { next.run(req).await })
}

fn api_mid<'a>(
	req: Request<()>,
	next: Next<'a, (), Request<()>, Result<Response, http::Error>>,
) -> Pin<Box<dyn Future<Output = Result<Response, http::Error>> + Send + 'a>> {
	log::info!("inside api middleware");

	Box::pin(async { next.run(req).await })
}

fn hello_mid<'a>(
	req: Request<()>,
	next: Next<'a, (), Request<()>, Result<Response, http::Error>>,
) -> Pin<Box<dyn Future<Output = Result<Response, http::Error>> + Send + 'a>> {
	log::info!("inside hello middleware");

	Box::pin(async { next.run(req).await })
}

#[async_std::main]
async fn main() -> async_std::io::Result<()> {
	std::env::set_var("RUST_LOG", "debug");
	env_logger::init();

	let mut app = Server::new();

	app.with(app_mid).await;

	app.at("/").get(|_| async { Ok("Root".into()) }).await;

	app.at("/api")
		.with(api_mid)
		.await
		.nest({
			let mut api = Server::new();
			api.at("/hello")
				.with(hello_mid)
				.await
				.get(|_| async { Ok("Hello, world".into()) })
				.await;
			api.at("/goodbye")
				.get(|_| async { Ok("Goodbye, world".into()) })
				.await;
			api
		})
		.await;

	app.listen("127.0.0.1:9090").await
}
