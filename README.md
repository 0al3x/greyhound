# greyhound

My personal backend framework

<!--
[//]: <> an extremally; small, minimal, modular, and hyper extensible, set of tools for backend development writen in rust
-->

<!--
[//]: <> # roadmap
[//]: <> 
[//]: <> ## primitives
[//]: <> - http primitives (rfc9110)
[//]: <> - http parsing
[//]: <> - http processing
[//]: <> - networking layer (http 1, 2, and 3 support (tcp, upd (quic)...))
[//]: <> 
[//]: <> ## basic framework
[//]: <> - http minimal framework (routing)
[//]: <> 
[//]: <> ## toolsets
[//]: <> - http middlewares toolset
[//]: <> - http auth toolset
[//]: <> - http database integration toolset
[//]: <> - http git networking toolset
[//]: <> - ssh management toolset
[//]: <> - static server layer toolset (proxies, gateways, cgi, etc)
[//]: <> 
[//]: <> 
[//]: <> # design goals
-->


This framework is hardly based on tide.
